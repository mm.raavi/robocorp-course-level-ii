import os
import zipfile
from robocorp.tasks import task
from robocorp import browser
from RPA.Excel.Files import Files as Excel
from RPA.Tables import Tables
from RPA.PDF import PDF
from RPA.HTTP import HTTP
from RPA.FileSystem import FileSystem

@task
def order_robots_from_RobotSpareBin():
    """
    Orders robots from RobotSpareBin Industries Inc.
    Saves the order HTML receipt as a PDF file.
    Saves the screenshot of the ordered robot.
    Embeds the screenshot of the robot to the PDF receipt.
    Creates ZIP archive of the receipts and the images.
    """
    browser.configure(
        slowmo=10,
        headless=False
    )
    orders = get_orders()
    open_robot_order_website()
    order_index = len(orders)-1
    failed_orders = []
    while order_index >= 0:
        close_annoying_modal()
        order = orders[order_index]
        print(order)
        order_index = order_index - 1
        fill_the_form(order)
        if preview_order():
            submit_order()
            receipt = get_receipt_as_html()
            pdf_file = store_receipt_as_pdf(order[0], receipt)
            embed_screenshot_to_receipt(order[0], pdf_file)
            order_another()
        else:
            order_index = order_index + 1
    archive_receipts()


def open_robot_order_website():
    """Navigates to the given URL"""
    browser.goto("https://robotsparebinindustries.com/#/robot-order")

def get_orders():
    # HTTP().download(url="https://robotsparebinindustries.com/orders.csv", overwrite=True)
    csv = Tables()
    table = csv.read_table_from_csv("orders.csv")
    return table

def close_annoying_modal():
    page = browser.page()
    page.click("text=Yep")

def fill_the_form(row):
    page = browser.page()
    page.select_option("#head", str(row[1])) # Head
    page.check(f'input[name="body"][value="{str(row[2])}"]') # Body
    page.fill('[placeholder="Enter the part number for the legs"]', str(row[3])) # Legs
    page.fill('#address', str(row[4])) # Address

def preview_order():
    page = browser.page()
    page.click("#preview")
    return page.is_visible("#robot-preview-image")

def submit_order():
    page = browser.page()
    page.click("#order")
    page.wait_for_timeout(1000)
    if page.locator('xpath=//div[@class="alert alert-danger"]').is_visible():
        message = page.locator('xpath=//div[@class="alert alert-danger"]').text_content()
        print(message)
        submit_order()

def get_receipt_as_html():
    receipt = None
    page = browser.page()
    if page.locator('id=receipt'):
        receipt = page.query_selector('#receipt').inner_html()
    print(receipt)
    return receipt

def order_another():
    page = browser.page()
    page.click('id=order-another')

def store_receipt_as_pdf(order_number, receipt):
    folder_output = os.path.join(os.getcwd(), 'output')
    folder_temp = os.path.join(folder_output, 'pdfs')
    filename_temp_text = os.path.join(folder_temp, f'text_{order_number}.pdf')
    pdf = PDF()
    fs = FileSystem()
    pdf.html_to_pdf(content=receipt, output_path=filename_temp_text)
    fs.wait_until_created(filename_temp_text)
    return filename_temp_text

def embed_screenshot_to_receipt(order_number, pdf_file):
    page = browser.page()
    pdf = PDF()
    fs = FileSystem()
    folder_output = os.path.join(os.getcwd(), 'output')
    folder_temp = os.path.join(folder_output, 'images')
    filename_temp_image = os.path.join(folder_temp, f'image{order_number}.png')
    page.locator("#robot-preview-image").screenshot(path=filename_temp_image)
    fs.wait_until_created(filename_temp_image)
    pdf.add_files_to_pdf(
        files = [
            pdf_file,
            filename_temp_image + ':align=center'
        ],
        target_document = pdf_file
    )

def archive_receipts():
    folder_output = os.path.join(os.getcwd(), 'output')
    folder_temp = os.path.join(folder_output, 'pdfs')
    folder_zip = os.path.join(folder_output, 'pdfs.zip')
    zip_folder(folder_temp, folder_zip)

def zip_folder(folder_path, zip_name):
    with zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED) as zipf:
        for root, dirs, files in os.walk(folder_path):
            for file in files:
                zipf.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                           os.path.join(folder_path, '..')))
